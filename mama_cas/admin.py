from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User
from .models import *

# Register your models here.

class DDSUserInline(admin.StackedInline):
    model = DDSUser
    can_delete = False
    verbose_name_plural = 'DDS Users'
    list_display = ['category', 'wrua_area']

# Define a new User admin
class UserAdmin(BaseUserAdmin):
    inlines = (DDSUserInline, )

# Re-register UserAdmin
admin.site.unregister(User)
admin.site.register(User, UserAdmin)