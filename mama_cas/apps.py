from __future__ import unicode_literals

from django.apps import AppConfig


class MamaCasConfig(AppConfig):
    name = 'mama_cas'
